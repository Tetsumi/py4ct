#
#   4CHAN API
#
#
#   Copyright 2013 Tetsumi <tetsumi@vmail.me>
# 
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import time
import urllib.request as req
import json

JSON_BOARDS_URL = "https://a.4cdn.org/boards.json"
JSON_PAGE_URL = "https://a.4cdn.org/{0}/{1}.json"
JSON_THREAD_URL = "https://a.4cdn.org/{0}/thread/{1}.json"


def get_page_url(board, page_number):
    return JSON_PAGE_URL.format(board.board, page_number)


def get_thread_url(board, thread_number):
    return JSON_THREAD_URL.format(board.board, thread_number)
    

def get_json (url):
    request = req.urlopen(url)
    txt = request.read().decode('utf-8')
    return json.loads(txt)


class Cooldowns:
    def __init__ (self, images, images_intra, replies, replies_intra, threads):
        self.images = images
        self.images_intra = images_intra
        self.replies = replies
        self.replies_intra = replies_intra
        self.threads = threads
        
        
class Board:
    boards = None
    UPDATE_INTERVAL = 30.0
    
    def __init__ (self, board_name):
        if not Board.boards:
            Board.__init_boards()
            
        for board in Board.boards:
            if board['board'] == board_name or board['title'] == board_name:
                break
        else:
            raise Exception("No such board as " + board_name)
            
        self.__dict__.update(board)
        self.cooldowns = Cooldowns(self.cooldowns['images'],
                                   self.cooldowns['images_intra'],
                                   self.cooldowns['replies'],
                                   self.cooldowns['replies_intra'],
                                   self.cooldowns['threads'])
        self.__threads = None
        self.next_update = 0
        
    def __init_boards ():
        jsondata = get_json(JSON_BOARDS_URL)
        Board.boards = jsondata['boards']
        
    @property
    def threads (self):
        current_time = time.time()
        if current_time > self.next_update:
            self.fetch()  
            self.next_update = current_time + Board.UPDATE_INTERVAL
        return self.__threads
        
    def fetch (self):
        threads = []
        
        for i in range(1, self.pages+1):
            url = get_page_url(self, i)
            jsondata = get_json(url)
            for thread in jsondata['threads']:
                threads.append(Thread(thread, self))
        
        self.__threads = threads
        
    def __str__ (self):
        return 'Board("{0}")'.format(self.title)
            
    def __repr__ (self):
        return 'Board("{0}") <{1}>'.format(self.title, hex(id(self)))

    
class Thread:
    UPDATE_INTERVAL = 30.0
    
    def __init__ (self, jsondata, board):
        self.__dict__.update(jsondata['posts'][0])
        self.__posts = None
        self.board = board
        self.next_update = 0
        
    @property
    def posts (self):
        current_time = time.time()
        if current_time > self.next_update:
            self.fetch()  
            self.next_update = current_time + Board.UPDATE_INTERVAL
        return self.__posts
        
    def fetch (self):
        posts = []
        url = get_thread_url(self.board, self.no)
        jsondata = get_json(url)
        for post in jsondata['posts'][1:]:
            posts.append(Post(post, self))
        self.__posts = posts
            
    def __str__ (self):
        return 'Thread [{0}]'.format(self.no)
            
    def __repr__ (self):
        return 'Thread [{0}] <{1}>'.format(self.no, hex(id(self)))
                                          
class Post:
    def __init__ (self, jsondata, thread):
        self.thread = thread
        self.board = thread.board
        self.__dict__.update(jsondata)
        
    def __str__ (self):
        return 'Post [{0}]'.format(self.no)
            
    def __repr__ (self):
        return 'Post [{0}] <{1}>'.format(self.no, hex(id(self)))
 
       
# Example: search the DPT thread
if __name__ == "__main__":
    print("Fetch /g/...")
    board = Board("g")
    print("Fetch threads")
    threads = board.threads
    print("Search for DPT")
    for thread in threads:
        try:
            sub = thread.sub.lower()
        except:
            continue
        
        if "dpt" in sub or "daily programming" in sub:
            break
    else:
        print("No DPT found.")
        import sys
        sys.exit(0)
        
    print("DPT found:", thread.no)